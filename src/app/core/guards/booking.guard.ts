import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {Router} from '@angular/router'
import {AuthService} from 'src/app/core/services/auth.service'

@Injectable({
  providedIn: 'root'
})
export class BookingGuard implements CanActivate {
  constructor(private auth : AuthService, private router : Router){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      const currentUser = this.auth.currentUser.value;


      if(currentUser){
        return true
      }

      (window as any).PATH = state.url

      alert('Bạn chưa đăng nhập')
      this.router.navigateByUrl('/signin')
    return false;
  }
  
}
