import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {SignupComponent} from 'src/app/main/signup/signup.component'

@Injectable({
  providedIn: 'root'
})
export class SignupGuard implements CanDeactivate<unknown> {
  canDeactivate(
    component: SignupComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      const isDirty = component.formDK.dirty && !component.formDK.submitted

      if(isDirty){
        const isCofirm = confirm('Bạn có chắc rời khỏi trang hay không, dữ liệu hiện tại sẽ mất');

        return isCofirm;
      }

    return true;
  }
  
}
