import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {AuthService} from 'src/app/core/services/auth.service'

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const currentUser = this.auth.currentUser.value

    if(currentUser){
      const {accessToken} = currentUser;

      request = request.clone({
        headers: request.headers.append('Authorization', `Beazer ${accessToken}`)
      })

    }


    return next.handle(request);
  }
}
