import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs'
import { HttpClient } from '@angular/common/http'
import { catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  baseUrl = `https://movie0706.cybersoft.edu.vn/api`;
  constructor( private http: HttpClient) { }

  handleError(err: any) {
    switch (err.status) {
      case 500:
        console.log('Lỗi Server')
        break;
      case 401:
        console.log('Lỗi Unauthorization')
        break;
      case 404: 
        console.log('Không tìm thấy tragn')
        break
      default:
        break;
    }

    return throwError(err)
  }

  get<T>(path: string, options = {}): Observable<T> {

    console.log(options)
    return this.http.get<T>(`${this.baseUrl}/${path}`, options).pipe(catchError(this.handleError))
  }

  post<T>(path: string, body: any, options = {}): Observable<T>{

    return this.http.post<T>(`${this.baseUrl}/${path}`, body, options).pipe(catchError(this.handleError))

  }

  put<T>(path: string, body: any, options = {}): Observable<T>{
    return this.http.put<T>(`${this.baseUrl}/${path}`, body, options).pipe(catchError(this.handleError))
  }

  delete<T>(path: string, options = {}): Observable<T>{
    return this.http.delete<T>(`${this.baseUrl}/${path}`, options).pipe(catchError(this.handleError))
  }
}
