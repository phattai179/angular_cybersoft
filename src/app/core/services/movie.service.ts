import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { Movie, MovieDetail } from 'src/app/core/models/movie'
import { ApiService } from './api.service'


@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http: HttpClient, private api: ApiService) { }

  getMovieList(): Observable<Movie[]> {
    // const url = `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim`

    // let params = new HttpParams()
    // params = params.append('maNhom', 'GP01')
    // return this.http.get<Movie[]>(url, {params})

    const url = `QuanLyPhim/LayDanhSachPhim`;
    let params = new HttpParams()
    params = params.append('maNhom', 'GP01');

    return this.api.get<Movie[]>(url, {params})


  }

  getMovieDetail(movieId: string): Observable<MovieDetail> {
    // const url = `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim`

    // let params = new HttpParams()
    // params = params.append('maPhim', movieId)

    // return this.http.get<MovieDetail>(url, { params })

    const url = `QuanLyPhim/LayThongTinPhim`;
    let params = new HttpParams()
    params = params.append('maPhim', movieId);

    return this.api.get<MovieDetail>(url, {params})

  }

  addMovie(values: any): Observable<any>{
    let url = `QuanLyPhim/ThemPhimUploadHinh`;

    let example = of('acv')
    const formData = new FormData()
    console.log('formData', formData)
    for(let item in values){
      formData.append(item, values[item]);
    }

    formData.append('maNhom', "GP01");
    console.log('formDataUpdate', formData)
    // return this.api.post<any>(url, formData)
    return example
  }


}
