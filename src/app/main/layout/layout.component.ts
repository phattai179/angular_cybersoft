import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import {AuthService} from 'src/app/core/services/auth.service';
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, OnDestroy {

  
  currentUser: any = null;
  // Subcription có hàm unsubscribe khi hk cần thay đổi
  currentUserSubcription : Subscription | null = null;

  constructor(private auth: AuthService) { }
    
  ngOnInit(): void {
    this.auth.currentUser.asObservable().subscribe({
      next: (result) => {
        this.currentUser = result
      }
    })
  }

  // Chạy trước khi component bị hủy
  ngOnDestroy(): void{
    console.log('destroy')
    this.currentUserSubcription?.unsubscribe()
  }

}
